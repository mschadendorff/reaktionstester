# Reaktionstester mit Arduino Uno

## Benutzerdoku

Der Arduino muss über ein 5V Netzteil, über eine USB Buchse von einem PC oder eine Batterie betrieben werden. Wenn der Aufbau richtig durchgeführt wurde, erscheint eine Animation, bei der jede LED von links nach rechts und anschließend von rechts nach links aufleuchtet. Der Reaktionstest kann über einen Knopfdruck gestartet werden. Daraufhin wird eine Startanimation in zufälliger Dauer angezeigt, bei der alle LEDs drei Mal in zufälliger Zeit aufleuchten. Nachdem die LEDs zum dritten Mal erloschen sind, muss der Knopf gedrückt werden. Schlussendlich wird die Reaktionszeit in Binärcode von den LEDs angezeigt (Reaktionszeiten bis 2^10 Millisekunden, also ungefähr eine Sekunde, können angezeigt werden).

## Zum Nachbauen (Technische Doku)

#### Aufbau
Zum Aufbau kann die vorhandene Fritzing Datei benutzt und ggf. erweitert werden.

#### Arudino - Code

Um den Code zu installieren, muss der Code (.ino Datei) auf den Arduino geflasht werden.
Dies geschieht über die Arduino IDE. Im Original wurde eine Arduino Uno benutzt. Theoretisch kann aber jeder andere Arduino benutzt werden. Dann könnten aber u.U. manuell Treiber zu installieren sein.
Der Ordner Reaktionstester im Ordner libraries enthält eine die "Reaktionstester"-Bibliothek (.cpp und .h Datei) mit notwendigen Funktion. Der Ordner muss in den lokalen "libraries" Ordner kopiert werden. Dieser befindet sich unter dem "Sketchbook-Speicherort". Dieser kann in den Einstellung der Arduino IDE eingesehen werden.
Unter Mac standardmäßig "/Users/[user]/Documents/Arduino". 
Es gibt keine bestehenden Bugs.
Das Hauptprogramm ist in der .ino Datei enthalten. Die notwendigen Funktionen sind in der Bibliothek "Reaktionstester" ausgelagert.