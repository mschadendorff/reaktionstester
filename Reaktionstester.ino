#include <Reaktionstester.h>
#include <ArduinoUnit.h>
const int button  = 12;

Reaktionstester Reaktionstester(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, button);

test(setAllLeds) {
  Reaktionstester.setAllLeds();
  assertEqual(digitalRead(2), HIGH);
  assertEqual(digitalRead(3), HIGH);
  assertEqual(digitalRead(4), HIGH);
  assertEqual(digitalRead(5), HIGH);
  assertEqual(digitalRead(6), HIGH);
  assertEqual(digitalRead(7), HIGH);
  assertEqual(digitalRead(8), HIGH);
  assertEqual(digitalRead(9), HIGH);
  assertEqual(digitalRead(10), HIGH);
  assertEqual(digitalRead(11), HIGH);
}

test(clearAllLeds) {
  Reaktionstester.clearAllLeds();
  assertEqual(digitalRead(2), LOW);
  assertEqual(digitalRead(3), LOW);
  assertEqual(digitalRead(4), LOW);
  assertEqual(digitalRead(5), LOW);
  assertEqual(digitalRead(6), LOW);
  assertEqual(digitalRead(7), LOW);
  assertEqual(digitalRead(8), LOW);
  assertEqual(digitalRead(9), LOW);
  assertEqual(digitalRead(10), LOW);
  assertEqual(digitalRead(11), LOW);
}

test(printResultBinary) {
  String result = Reaktionstester.printResultBinary(100);
  Serial.print(result);
  // assertEqual(result,"100ms Reaktionszeit Als Binärzahl: 00 1100100");
  assertEqual(digitalRead(2), LOW);
  assertEqual(digitalRead(3), LOW);
  assertEqual(digitalRead(4), HIGH);
  assertEqual(digitalRead(5), LOW);
  assertEqual(digitalRead(6), LOW);
  assertEqual(digitalRead(7), HIGH);
  assertEqual(digitalRead(8), HIGH);
  assertEqual(digitalRead(9), LOW);
  assertEqual(digitalRead(10), LOW);
  assertEqual(digitalRead(11), LOW);
}

void setup() {
  Serial.begin(9600);
  Reaktionstester.startAnimation();
}

void loop() {
  Test::run();
  if (digitalRead(button) == HIGH) {    

    Reaktionstester.startTest();

    // wait for a short while so button is not triggerd again
    delay(500); 
  }
}



