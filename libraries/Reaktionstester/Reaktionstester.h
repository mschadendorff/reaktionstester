/*
 * Bibliothek um einen einfachen Reaktionstester zu realisieren.
 */

#ifndef Reaktionstester_h
#define Reaktionstester_h

class Reaktionstester {
public:

    // need to be given in the correct order
    Reaktionstester(int pin1, int pin2, int pin3, int pin4, int pin5,
                    int pin6, int pin7, int pin8, int pin9, int pin10, int button);

    void startAnimation();

    void startTest();

    void setAllLeds();

    void clearAllLeds();

    char* printResultBinary(unsigned int reactionTime);


private:
    const static int ARR_SIZE = 10;
    int leds[ARR_SIZE];
    int _button;

    unsigned int waitForResult();
};

#endif

