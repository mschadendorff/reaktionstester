/*
 * Bibliothek um einen einfachen Reaktionstester zu realisieren.
 */
#include "Arduino.h"
#include "Reaktionstester.h"

Reaktionstester::Reaktionstester(int pin1, int pin2, int pin3, int pin4, int pin5,
                                 int pin6, int pin7, int pin8, int pin9, int pin10, int button) {
    leds[0] = pin1;
    leds[1] = pin2;
    leds[2] = pin3;
    leds[3] = pin4;
    leds[4] = pin5;
    leds[5] = pin6;
    leds[6] = pin7;
    leds[7] = pin8;
    leds[8] = pin9;
    leds[9] = pin10;
    _button = button;

    pinMode(pin1, OUTPUT);
    pinMode(pin2, OUTPUT);
    pinMode(pin3, OUTPUT);
    pinMode(pin4, OUTPUT);
    pinMode(pin5, OUTPUT);
    pinMode(pin6, OUTPUT);
    pinMode(pin7, OUTPUT);
    pinMode(pin8, OUTPUT);
    pinMode(pin9, OUTPUT);
    pinMode(pin10, OUTPUT);
    pinMode(button, INPUT);
}

void Reaktionstester::startAnimation() {
    int waitTime = 100;

    for (int i = 0; i < ARR_SIZE; i++) {
        digitalWrite(leds[i], HIGH);
        delay(waitTime);
        digitalWrite(leds[i], LOW);
    }

    for (int i = ARR_SIZE - 1; i >= 0; i--) {
        digitalWrite(leds[i], HIGH);
        delay(waitTime);
        digitalWrite(leds[i], LOW);
    }
}

void Reaktionstester::startTest() {
    int reactionTime = 0;

    for (int i = 0; i < 3; i++) {
        setAllLeds();
        delay(random(500, 3000));
        clearAllLeds();

        if (i != 2) {
            delay(random(500, 3000));
        }
    }

    reactionTime = waitForResult();

    String result = printResultBinary(reactionTime);
    Serial.println(result);
}

unsigned int Reaktionstester::waitForResult() {
    unsigned int begin = millis();
    unsigned int reactionTime;
    boolean pressed = false;

    while (!pressed) {
        if (digitalRead(_button) == HIGH) {
            reactionTime = millis() - begin;
            pressed = true;
        }
    }

    return reactionTime;
}

/*
 * prints the first reaction time on serial monitor and on the leds (until 1024)
 * Byte order: Big endian
 */
char* Reaktionstester::printResultBinary(unsigned int reactionTime) {
    // print on serial monitor
    int idx = 0;
    String reactionTimeString;
    String result = String(reactionTime);
    result += reactionTimeString;
    result += "ms Reaktionszeit";
    result += "Als Binärzahl: ";

    for (idx = sizeof(reactionTime) * 8 - 1; idx >= 0; idx--) {
        result += ((reactionTime & (1 << idx)) ? "1" : "0");
        result += ((idx % 8) ? "" : " ");
    }

    // print on leds
    for (int i = 0; i < ARR_SIZE; i++) {
        digitalWrite(leds[ARR_SIZE - 1 - i], reactionTime & (1 << i) ? HIGH : LOW);
    }

    delay(5000);

    clearAllLeds();
}

void Reaktionstester::setAllLeds() {
    for (int i = 0; i < ARR_SIZE; i++) {
        digitalWrite(leds[i], HIGH);
    }
}

void Reaktionstester::clearAllLeds() {
    for (int i = 0; i < ARR_SIZE; i++) {
        digitalWrite(leds[i], LOW);
    }
}
 
